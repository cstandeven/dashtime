/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/
/**
 * Simple Time skill to show AWS automation
 **/

'use strict';

const Alexa = require('alexa-sdk');

const APP_ID = undefined;  // TODO replace with your app ID (OPTIONAL).

const preMessage = 'The current time is ';
//const postMessage = '';
//const postMessage = ' and this is the first post message';
const postMessage = ' and you should hire Curtis Standeven';

const time = new Date();

const handlers = {
    'LaunchRequest': function () {
        this.emit('DashTime');
    },
    'DashTimeIntent': function () {
        this.emit('DashTime');
    },
    'DashTime': function () {
        // Get the current time, preceded by a pre-message
        //var hours = (time.getHours() - 5) % 12; // Convert UTC to EST
        var hours = time.getHours() - 5; // Convert UTC to EST
		hours = hours < 1 ? (hours + 24) : hours;
        var ampm = hours > 12 ? "p m" : "a m";
		hours = hours % 12;
        hours = hours ? hours : 12;
        var minutes = time.getMinutes();
        minutes = minutes < 10 && minutes > 0 ? "o" + minutes : minutes;
        minutes = minutes == 0 ? " " : minutes;
        const currentTime = hours + " " + minutes + ampm;
        var delay = " ";
        if (postMessage.length > 0) {
            delay = '<break time="1s"/>';
        }
        
        const speechOutput =  preMessage + currentTime + delay + postMessage;
        this.emit(':tellWithCard', speechOutput, this.t('SKILL_NAME'), speechOutput);
    },
    'AMAZON.HelpIntent': function () {
        const speechOutput = this.t('HELP_MESSAGE');
        const reprompt = this.t('HELP_MESSAGE');
        this.emit(':ask', speechOutput, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell', this.t('STOP_MESSAGE'));
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', this.t('STOP_MESSAGE'));
    },
};

exports.handler = function (event, context) {
    const alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    // To enable string internationalization (i18n) features, set a resources object.
    alexa.registerHandlers(handlers);
    alexa.execute();
};
