# update-aws Cookbook
- Update the code on AWS (Lambda, EC2, etc)

## Requirements

### Platforms

- Linux

### Chef

- Chef 12.0 or later


## Usage

### update-aws::default

Just include `update-aws` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[update-aws]"
  ]
}
```

## License and Authors
- Authors: Curtis Standeven

