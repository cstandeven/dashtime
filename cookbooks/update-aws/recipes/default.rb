# Cookbook Name:: update-aws
# Recipe:: default
#
# Copyright 2017, Curtis Standeven
#
# All rights reserved - Do Not Redistribute
#

## AWS lambda code directoy
directory "/home/pi/nfsDemo/gitclone/aws/DashTime" do
	owner "pi"
	mode "0755"
	action :create
end

## Bash script to download AWS lambda function code
template "getLambdaCode.sh" do
	path "/home/pi/nfsDemo/gitclone/bin/getLambdaCode.sh"
	source "getLambdaCode.sh"
	owner "pi"
	group "pi"
	mode "0744"
end

## Ruby script to upload AWS lambda function code
template "awsUpload.rb" do
	path "/home/pi/nfsDemo/gitclone/bin/awsUpload.rb"
	source "awsUpload.rb"
	owner "pi"
	group "pi"
	mode "0744"
end

## Execute the download script only once
execute "getLambdaCode.sh" do
	command "/home/pi/nfsDemo/gitclone/bin/getLambdaCode.sh DashTime"
	not_if 'test -e /tmp/.token'
end

## Create token which will be used to determine last deployment
file "/tmp/.token" do
	owner "pi"
	group "pi"
	mode "0600"
	action :create_if_missing
end

## Execute the AWS upload code
execute "awsUpload.rb" do
	command "/home/pi/nfsDemo/gitclone/bin/awsUpload.rb"
end

