#!/usr/bin/env ruby
# Name: awsUpload
# Description: Upload code to AWS Lambda if updates were made
#
# Copyright 2017, Curtis Standeven
#
# All rights reserved - Do Not Redistribute

require 'aws-sdk'
require 'aws-sdk-lambda'
require 'json'
require 'fileutils'

## Look for changes made to the AWS lambda code at the workstation
result=""
if File.directory?("/home/pi/nfsDemo/gitclone/aws/DashTime") then
	if File.exists?("/tmp/.token") then
		Dir.chdir("/home/pi/nfsDemo/gitclone/aws/DashTime") do
			result=%x(find . -anewer /tmp/.token -type f)
		end
		FileUtils.touch("/tmp/.token")
	end
end

puts result.length
puts result

## Push changes to AWS Lambda
if result.length > 0 then
	## File containing access keys
	secrets = "#{Dir.home}/secrets.json"

	## Load config
	lambda = JSON.load(File.new(secrets))
	Aws.config[:credentials] = Aws::Credentials.new(lambda['credentials']['access_key_id'], lambda['credentials']['secret_access_key'])
	Aws.config[:region] = lambda['region']

	client = Aws::LambdaPreview::Client.new()

	## Create zip of local version of the Alexa skill
	tempfile="/tmp/#{Process.pid}.zip"
	Dir.chdir("/home/pi/nfsDemo/gitclone/aws/DashTime") do
		system("/usr/bin/zip -r #{tempfile} .")
	end
	
	## Upload the new version of the Alexa skill
	zipfile = File.new(tempfile)
	resp = client.upload_function({
		function_name: "DashTime", # required
		function_zip: zipfile, # file/IO object, or string data, required
		runtime: "nodejs6.10", # required, accepts nodejs
		role: "arn:aws:iam::114287118806:role/lambda_basic_execution", # required
		handler: "index.handler", # required
		mode: "event", # required, accepts event
		description: "Alexa skill to demonstrate Chef automation",
})
end

## Push changes to AWS EC2 instance
#scp -i ~/.chef/DemoKey.pem -o StrictHostKeyChecking=no /tmp/.monitor ec2-user@ec2:/tmp/
if File.exists?("/home/pi/.chef/DemoKey.pem") then
	if File.exists?("/tmp/.dash") then
		dashcount = File.read("/tmp/.dash")
		File.write("/tmp/.monitor", "#{Time.now}: Dash button press count is #{dashcount}")
		system("scp -i /home/pi/.chef/DemoKey.pem -o StrictHostKeyChecking=no /tmp/.monitor ec2-user@ec2:/tmp/")
	end
else
	abort("ERROR: missing DemoKey.pem in /home/pi/.chef/")
end
