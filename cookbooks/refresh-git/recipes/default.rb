# Cookbook Name:: refresh-git
# Recipe:: default
#
# Copyright 2017, Curtis Standeven
#
# All rights reserved - Do Not Redistribute
#

## SSH wrapper for the git request
file "/tmp/git_ssh_wrapper.sh" do
  owner "root"
  mode "0755"
  content "#!/bin/sh\nexec /usr/bin/ssh -o \"StrictHostKeyChecking=no\" -i " + node[:git][:rsa] + " \"$@\""
end

## Get latest code from the git repo
git node[:git][:dir] do
  repository node[:git][:repo]
  reference node[:git][:branch]
  action :sync
  ssh_wrapper "/tmp/git_ssh_wrapper.sh"
end

execute 'knife_refresh_git' do
	  command 'knife cookbook upload refresh-git'
end

execute 'knife_update_aws' do
	  command 'knife cookbook upload update-aws'
end
