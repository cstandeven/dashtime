# refresh-git Cookbook
- Refresh Git checkouts on the node

## Requirements

### Platforms

- Linux

### Chef

- Chef 12.0 or later


## Attributes

### Local machine
- default[:git][:dir]
- default[:git][:rsa]

### Remote git
- default[:git][:repo]
- default[:git][:branch]


## Usage

### refresh-git::default

Just include `refresh-git` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[refresh-git]"
  ]
}
```

## License and Authors
- Authors: Curtis Standeven

