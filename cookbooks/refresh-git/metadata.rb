name             'refresh-git'
maintainer       'Curtis S'
maintainer_email 'cstandeven@gmail.com'
license          'All rights reserved'
description      'Installs/Configures refresh-git'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
