# Attributes for our git 
#
# Local machine
default[:git][:dir]    = "/home/pi/nfsDemo/gitclone" 
default[:git][:rsa]    = "/home/pi/.ssh/id_rsa"

# Remote git
default[:git][:repo]   = "git@bitbucket.org:cstandeven/dashtime.git"
default[:git][:branch] = "master"
