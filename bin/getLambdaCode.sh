#!/bin/bash
# Name: getLambdaCode
# Description: Download AWS Lambda function code
#
# Copyright 2017, Curtis Standeven
#
# All rights reserved - Do Not Redistribute

working="/tmp/$$.zip"
dest="/home/pi/nfsDemo/gitlocal/aws"
lambda="$1"
token="/tmp/.token"

# Function to print error and clean up
function earlyexit
{
  echo
  echo $1
  if [ -e "${working}" ]; then
    rm ${working}
  fi
  exit 1
}

## Usage statement 
if [ $# -lt 1 ]; then
  earlyexit "USAGE: $0 <lambda function>"
fi

echo "Downloading the AWS lambda function code for ${lambda}"
json="$(/usr/local/bin/aws lambda get-function --function-name ${lambda} 2>/dev/null)"

## Lambda function request returned empty
if [ $? -ne 0 ]; then
  earlyexit "ERROR: AWS lambda function does not exist"
else
  echo "  Received JSON tree"
fi

## Parse JSON returned from lambda function request
address="$(echo ${json} | sed -e 's|.*"Location": "||' -e 's|".*||')"
curl "${address}" -o ${working} &>/dev/null

## AWS lambda function code did not download
if [ ! -e "${working}" ]; then
  earlyexit "ERROR: problem downloading the AWS lambda function code"
else
  echo "  Downloaded the AWS lambda function code"
fi

## Finally make directory to store local code
mkdir -p ${dest}/${lambda}
if [ ! -e "${dest}/${lambda}" ]; then
  earlyexit "ERROR: unable to create local directory"
else
  echo "  Created the local storage directory"
fi

## Unzip to the destination directory
unzip -u ${working} -d ${dest}/${lambda}/ &>/dev/null
if [ $? -ne 0 ]; then
  earlyexit "ERROR: unable to extract the AWS lambda function code"
else
  echo "  Extracted the AWS lambda function code"
  echo "SUCCESS"
  echo
fi

## Touch the token file, used to determine when updates occur
touch ${token}
