#!/bin/bash
# Name: monitorChanges
# Description: Monitor file for changes on EC2 instance
#
# Copyright 2017, Curtis Standeven
#
# All rights reserved - Do Not Redistribute

## Check to see if the monitor file exists, otherwise create
if [ ! -e /tmp/.monitor ]; then
	echo "--> Initial monitor file state" > /tmp/.monitor
fi

## Watch for changes to the monitor file
watch -d -n 5 'cat /tmp/.monitor'
